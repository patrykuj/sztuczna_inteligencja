#include "utils.hh"

#include <SFML/System/Vector2.hpp>
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <iterator>
#include <vector>

#include "bullet.hh"
#include "enemy.hh"
#include "globals.hh"
#include "obstacle.hh"
#include "player.hh"

std::vector<Obstacle> obstacles;
Player player;
std::vector<Enemy> enemies;
std::vector<Bullet> bullets;

float fRand(float fMin, float fMax) {
	float f = (float) rand() / RAND_MAX;
	return fMin + f * (fMax - fMin);
}

sf::Vector2f getRandomPosition(int width, int height) {
	return sf::Vector2f(rand() % width, rand() % height);
}

float distance(sf::Vector2f a, sf::Vector2f b) {
	float x = a.x - b.x;
	float y = a.y - b.y;
	return sqrtf(x * x + y * y);
}

bool circleCollision(sf::Vector2f positionA, float radiusA, sf::Vector2f positionB, float radiusB, float scale) {
	float radius = radiusA + radiusB;
	if(distance(positionA, positionB) > radius * scale)
		return false;
	return true;
}

bool checkObstacle(std::vector<Obstacle> obstacle, sf::Vector2f position, float radius) {
	float x = position.x;
	float y = position.y;
	float radius2 = radius + PLAYER_SIZE * 2.0f;
	if(x - radius2 < 0.0f || y - radius2 < 0.0f || x + radius2 >= width || y + radius2 >= height) {
		return false;
	}
	for(std::vector<Obstacle>::iterator it = obstacle.begin(); it != obstacle.end(); ++it) {
		if(circleCollision((*it).getPosition(), (*it).getRadius(), position, radius, 2.0f))
			return false;
	}
	return true;
}

bool checkEnemy(std::vector<Enemy> enemies, sf::Vector2f position, float radius) {
	float x = position.x;
	float y = position.y;
	float radius2 = radius + PLAYER_SIZE * 2.0f;
	if(x - radius2 < 0.0f || y - radius2 < 0.0f || x + radius2 >= width || y + radius2 >= height) {
		return false;
	}
	for(std::vector<Enemy>::iterator it = enemies.begin(); it != enemies.end(); ++it) {
		if(circleCollision((*it).getPosition(), (*it).getRadius(), position, radius, 2.0f))
			return false;
	}
	return true;

}

bool triangleCircleCollision(std::vector<sf::Vector2f> &triangle, sf::Vector2f circleCenter, float radius) {
	float radiusSqr = radius * radius;

	float c0x = circleCenter.x - triangle[0].x;
	float c0y = circleCenter.y - triangle[0].y;
	float c0sqr = c0x * c0x + c0y * c0y - radiusSqr;
	if(c0sqr <= 0)
		return true;

	float c1x = circleCenter.x - triangle[1].x;
	float c1y = circleCenter.y - triangle[1].y;
	float c1sqr = c1x * c1x + c1y * c1y - radiusSqr;
	if(c1sqr <= 0)
		return true;

	float c2x = circleCenter.x - triangle[2].x;
	float c2y = circleCenter.y - triangle[2].y;
	float c2sqr = c2x * c2x + c2y * c2y - radiusSqr;
	if(c2sqr <= 0)
		return true;

	float e0x = triangle[1].x - triangle[0].x;
	float e0y = triangle[1].y - triangle[0].y;
	float e1x = triangle[2].x - triangle[1].x;
	float e1y = triangle[2].y - triangle[1].y;
	float e2x = triangle[0].x - triangle[2].x;
	float e2y = triangle[0].y - triangle[2].y;

	if(e0y * c0x >= e0x * c0y && e1y * c1x >= e1x * c1y && e2y * c2x >= e2x * c2y)
		return true;

	float k = c0x * e0x + c0y * e0y;
	if(k > 0) {
		float len = e0x * e0x + e0y * e0y;

		if(k < len) {
			if(c0sqr * len <= k * k)
				return true;
		}
	}

	k = c1x * e1x + c1y * e1y;
	if(k > 0) {
		float len = e1x * e1x + e1y * e1y;

		if(k < len) {
			if(c1sqr * len <= k * k) {
				return true;
			}
		}
	}

	k = c2x * c2x + c2y * c2y;

	if(k > 0) {
		float len = e2x * e2x + e2y * e2y;

		if(k < len) {
			if(c2sqr * len <= k * k) {
				return true;
			}
		}
	}

	return false;
}

bool rectangleCircleCollision(std::vector<sf::Vector2f> &rectangle, sf::Vector2f circleCenter, float radius) {
	std::vector<sf::Vector2f>::const_iterator it = rectangle.begin();
	float dist = distance(*it, circleCenter);
	++it;
	for(; it != rectangle.end(); ++it) {
		float newDistance = distance(*it, circleCenter);
		if(newDistance < dist) {
			dist = newDistance;
		}
	}

	if(dist <= radius)
		return true;
	else
		return false;
}

void prepareObstacle() {
	obstacles.clear();
	int obstacleCount = (width * height) / 52500;
	for(int i = 0; i < obstacleCount; ++i) {
		sf::Vector2f position = getRandomPosition(width, height);
		float radius = rand() % 25 + 30.0f;
		while(!checkObstacle(obstacles, position, radius)) {
			position = getRandomPosition(width, height);
			radius = rand() % 25 + 30.0f;
		}
		obstacles.push_back(Obstacle(position, radius));
	}
}

void preparePlayer() {
	sf::Vector2f position = getRandomPosition(width, height);
	while(!checkObstacle(obstacles, position, PLAYER_SIZE)) {
		position = getRandomPosition(width, height);
	}

	player = Player(position, PLAYER_SIZE);
}

bool playerSafeRespawnZone(sf::Vector2f startCoordinates) {
	sf::Vector2f playerPosition = player.getPosition();
	float safeZoneRadius = ENEMY_SIZE + PLAYER_SIZE + SIZE_OF_PLAYER_SAFE_RESAWN_ZONE;
	if(distance(playerPosition, startCoordinates) < safeZoneRadius)
		return true;
	return false;
}

void prepareEnemy() {
	enemies.clear();

	sf::Vector2f startCoordinates = getRandomPosition(width, height);
	for(int i = 0; i < ENEMY_COUNT; i++) {
		startCoordinates = getRandomPosition(width, height);
		while(!checkObstacle(obstacles, startCoordinates, ENEMY_SIZE) || !checkEnemy(enemies, startCoordinates, ENEMY_SIZE) || playerSafeRespawnZone(startCoordinates)) {
			startCoordinates = getRandomPosition(width, height);
		}
		enemies.push_back(Enemy(startCoordinates, ENEMY_SIZE));
	}
}

void prepareGame() {
	bullets.clear();
	prepareObstacle();
	preparePlayer();
	prepareEnemy();
}

void updateEnemy() {
	for(std::vector<Enemy>::iterator it = enemies.begin(); it != enemies.end(); ++it) {
		it->update();
	}
}

void updateBullet() {
	for(std::vector<Bullet>::iterator it = bullets.begin(); it != bullets.end();) {
		if(it->update()) {
			it = bullets.erase(it);
		} else {
			++it;
		}
	}
}

void updateObstacle() {
	for(std::vector<Obstacle>::iterator it = obstacles.begin(); it != obstacles.end(); ++it) {
		it->update();
	}
}

void updateGame() {
	updateEnemy();
	player.update();
	updateBullet();
	updateObstacle();
}

float getMagnitude(sf::Vector2f velocity) {
	return (float) sqrt(velocity.x * velocity.x + velocity.y * velocity.y);
}

sf::Vector2f normalize(sf::Vector2f e) {
	float magnitude = getMagnitude(e);
	if(magnitude == 0.0)
		magnitude = 0.000001;

	e.x /= magnitude;
	e.y /= magnitude;
	return e;
}
