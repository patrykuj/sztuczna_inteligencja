#include "enemy.hh"

#include <SFML/Graphics/Rect.hpp>
#include <cmath>
#include <cstdlib>

#include "globals.hh"
#include "healthbar.hh"
#include "player.hh"
#include "utils.hh"

Enemy::Enemy(float radius) :
		ObjectCircleShape(radius), Character(100, 100, 0, 0, 2 * radius), steeringBehaviour(SteeringBehaviour()), texture(sf::Texture()), botCounter(0) {
	dis = 3.0;
	wanderAngle = rand() % 360 / DEGREES;
	texture.loadFromFile("img/enemy.png", sf::IntRect(0, 0, 30, 30));
	texture.setSmooth(true);
}

Enemy::Enemy(sf::Vector2f position, float radius) :
		Enemy(radius) {
	this->position = position;
	this->healthBar.setPosition(position.x, position.y - radius - HEALTH_BAR_OFFSET);
	setPosition(this->position);

}

Enemy::Enemy(float x, float y, float radius) :
		Enemy(radius) {
	this->position = sf::Vector2f(x, y);
	this->healthBar.setPosition(x, y - radius - HEALTH_BAR_OFFSET);
	setPosition(this->position);
}

void Enemy::update() {
	//float distanceToPlayer;
	sf::Vector2f bounce = steeringBehaviour.getVectorFieldValue(position);
	switch(steeringBehaviour.b) {
		case WANDER:
			steeringBehaviour.wander(velocity, wanderAngle, position, bounce);
			//distanceToPlayer = distance(position, player.getPosition());
			if(distance(position, player.getPosition()) < ENEMY_SAFE_ZONE)
				steeringBehaviour.b = HIDE;
			break;
		case HIDE:
			steeringBehaviour.getHidingPosition(position, velocity, bounce);
			if(steeringBehaviour.isDestinationReach(position, steeringBehaviour.destination)) {
				if(distance(position, player.getPosition()) > ENEMY_SAFE_ZONE)
					steeringBehaviour.b = WANDER;
				steeringBehaviour.destinationReach = true;
			}
			break;
		case ATTACK:
			steeringBehaviour.attack(position, velocity, bounce, player.getPosition(), player.getVelocity());
			break;
	}

	setPosition(position);
	updateHealthBar();
	setTexture(&texture);
	attackCheck();
}

void Enemy::attackCheck() {
	float distanceBetweenEnemies;
	//int tab[3] = {0};
	//std::vector<Enemy>::iterator it3 = enemies.end();
//	for(std::vector<Enemy>::iterator it = enemies.begin(); it != enemies.end(); ++it){
//		distanceBetweenEnemies = distance(it->getPosition(), it->getPosition());
//		if( distanceBetweenEnemies < ATTACK_CIRCLE && distanceBetweenEnemies > DESTINATION_REACH){
//
//		}
//	}

	for(std::vector<Enemy>::iterator it = enemies.begin(); it != enemies.end(); ++it) {

		std::vector<Enemy>::iterator it3 = enemies.end();
		bool more = false;

		for(std::vector<Enemy>::iterator it2 = enemies.begin(); it2 != enemies.end(); ++it2) {
			distanceBetweenEnemies = distance(it->getPosition(), it2->getPosition());
			if(distanceBetweenEnemies < ATTACK_CIRCLE && distanceBetweenEnemies > DESTINATION_REACH) {
				//it->botCounter++;
				if(it3 == enemies.end()) {
					it3 = it2;
				} else {
					it2->steeringBehaviour.b = ATTACK;
					more = true;
				}
			}
//			if(it->botCounter >= 3){
//				it->steeringBehaviour.b = ATTACK;
//				continue;
//			}
		}
		if(it3 != enemies.end() && more) {
			it3->steeringBehaviour.b = ATTACK;
			it->steeringBehaviour.b = ATTACK;
		}
		//it->botCounter = 0;
	}
}

void Enemy::updateHealthBar() {
	float barY = position.y - radius - HEALTH_BAR_OFFSET;
	healthBar.setPosition(position.x, barY > 0 ? barY : position.y + radius + HEALTH_BAR_OFFSET);
}

float Enemy::GetMagnitude(sf::Vector2f vec) {
	return (float) sqrt(vec.x * vec.x + vec.y * vec.y);
}

sf::Vector2f Enemy::Normalize(sf::Vector2f vec) {
	float magnitude = GetMagnitude(vec);
	if(magnitude == 0.0)
		magnitude = 0.000001;

	vec.x /= magnitude;
	vec.y /= magnitude;

	return vec;
}
