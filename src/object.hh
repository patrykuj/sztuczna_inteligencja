#ifndef OBJECT_HH_
#define OBJECT_HH_

#include <SFML/System/Vector2.hpp>

class Object {
	protected:
		sf::Vector2f position;
};

#endif /* OBJECT_HH_ */
