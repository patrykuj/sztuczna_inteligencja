#ifndef OBSTACLE_HH_
#define OBSTACLE_HH_

#include <SFML/Graphics/Texture.hpp>
#include <SFML/System/Vector2.hpp>

#include "object.hh"
#include "objectcircleshape.hh"

class Obstacle : Object, public ObjectCircleShape {
	private:
		sf::Texture texture;
		Obstacle(float radius);
	public:
		Obstacle(sf::Vector2f position, float radius = 40.0f);
		Obstacle(float x, float y, float radius = 40.0f);
		void update();
};

#endif /* OBSTACLE_HH_ */
