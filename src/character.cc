#include "character.hh"

Character::Character(int hitpoints, int maxHitpoints, int x, int y, float width, float height) :
		velocity(sf::Vector2f(0.0f, 0.0f)), orientation(0.0f), angleularVelocity(0.0f), orientationAngle(0.0f), healthBar(HealthBar(hitpoints, maxHitpoints, x, y, width, height)) {

}

Character::~Character() {

}

HealthBar Character::getHealthBar() {
	return healthBar;
}

bool Character::heal(int hitpoints) {
	return healthBar.heal(hitpoints);
}
bool Character::hit(int hitpoints) {
	return healthBar.hit(hitpoints);
}
int Character::getHitpoints() {
	return healthBar.getHitpoints();
}
