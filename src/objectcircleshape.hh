#ifndef OBJECTCIRCLESHAPE_HH_
#define OBJECTCIRCLESHAPE_HH_

#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/System/Vector2.hpp>
#include <vector>

class ObjectCircleShape : public sf::CircleShape {
	protected:
		float radius;
	public:
		ObjectCircleShape(float radius = 15.0f, unsigned int pointCount = 30);
		sf::Vector2f getVertexPoint(unsigned int index);
		std::vector<sf::Vector2f> getVertex();
};

#endif /* OBJECTCIRCLESHAPE_HH_ */
