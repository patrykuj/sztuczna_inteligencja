#include "bullet.hh"

#include <cmath>
#include <iterator>
#include <vector>
#include <iostream>

#include "character.hh"
#include "globals.hh"
#include "obstacle.hh"
#include "utils.hh"

Bullet::Bullet() :
		Bullet(sf::Vector2f(0.0f, 0.0f), 0.0f) {
}

Bullet::Bullet(sf::Vector2f position, float rotation) :
		sf::RectangleShape(BULLET_SIZE), position(position), rotation(rotation) {
	float x = BULLET_THICKNESS / 2;
	float y = BULLET_LENGTH / 2;
	setRotation(rotation);
	setOrigin(x, y);
	setPosition(this->position);
	float orientation = rotation / DEGREES;
	velocity = sf::Vector2f(velocity.x += sin(orientation) * BULLET_SPEED, velocity.y -= cos(orientation) * BULLET_SPEED);
}

Bullet::Bullet(float positionX, float positionY, float rotation) :
		Bullet(sf::Vector2f(positionX, positionY), rotation) {
}

bool Bullet::update() {
	position += velocity;
	setPosition(position);
	std::vector<sf::Vector2f> vertex;

	for(unsigned int i = 0; i < 4; ++i) {
		vertex.push_back(getTransform().transformPoint(getPoint(i)));
	}

	for(std::vector<Enemy>::iterator it = enemies.begin(); it != enemies.end();) {
		if(rectangleCircleCollision(vertex, it->getPosition(), it->getRadius())) {
			it->hit(rand() % (MAX_BULLET_HITPOINT - MIN_BULLET_HITPOINT) + MIN_BULLET_HITPOINT);
			if(it->getHitpoints() <= 0)
				it = enemies.erase(it);
			return true;
		} else
			++it;
	}

	if(position.x < 0 || position.x > width || position.y < 0 || position.y > height) {
		return true;
	}

	float len = BULLET_LENGTH / 2;

	for(std::vector<Obstacle>::iterator it = obstacles.begin(); it != obstacles.end(); ++it) {
		if(circleCollision(position, len, it->getPosition(), it->getRadius()))
			return true;
	}

	return false;
}
