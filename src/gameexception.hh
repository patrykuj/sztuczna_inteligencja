#ifndef GAMEEXCEPTION_HH_
#define GAMEEXCEPTION_HH_

#include <stdexcept>
#include <string>

class GameException : public std::runtime_error {
	public:
		GameException(const std::string& what_arg);
		GameException(const char* what_arg);
};

#endif /* GAMEEXCEPTION_HH_ */
