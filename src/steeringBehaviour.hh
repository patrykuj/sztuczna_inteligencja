#ifndef STEERINGBEHAVIOUR_HH_
#define STEERINGBEHAVIOUR_HH_

#include <SFML/System/Vector2.hpp>

enum Behaviour {
	WANDER, HIDE, ATTACK
};

class SteeringBehaviour {
	public:
		bool destinationReach;
		sf::Vector2f destination;
		Behaviour b;
		SteeringBehaviour();
		void wander(sf::Vector2f &velocity, float &wanderAngle, sf::Vector2f &position, sf::Vector2f bounce);
		bool collisionTest(sf::Vector2f position);
		bool EnemyCollision(sf::Vector2f positionA, float radiusA, sf::Vector2f positionB, float radiusB, float rad);
		sf::Vector2f getVectorFieldValue(sf::Vector2f &position);
		void getHidingPosition(sf::Vector2f &position, sf::Vector2f &velocity, sf::Vector2f bounce);
		bool isDestinationReach(sf::Vector2f position, sf::Vector2f destination);
		void endOfScreen(sf::Vector2f &position, sf::Vector2f &velocity, sf::Vector2f bounce);
		void attack(sf::Vector2f &position, sf::Vector2f &velocity, sf::Vector2f bounce, sf::Vector2f playerPos, sf::Vector2f playerVelocity);
};

#endif /* STEERINGBEHAVIOUR_HH_ */
