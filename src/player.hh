#ifndef PLAYER_HH_
#define PLAYER_HH_

#include <SFML/Graphics/Texture.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/System/Time.hpp>
#include <SFML/System/Vector2.hpp>

#include "character.hh"
#include "objectcircleshape.hh"

class Player : public ObjectCircleShape, public Character {
	private:
		sf::Clock timer;
		sf::Time frequency = sf::milliseconds(500);
		sf::Texture texture;

		Player(float radius);
		sf::Vector2f getVectorFieldValue();
		void updateHealthBar();
	public:
		Player();
		Player(sf::Vector2f position, float radius = 15.0f);
		Player(float x, float y, float radius = 15.0f);
		void update();
		sf::Vector2f getVelocity();
};

#endif /* PLAYER_HH_ */
