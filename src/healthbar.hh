#ifndef HEALTHBAR_HH_
#define HEALTHBAR_HH_

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/System/Vector2.hpp>

class HealthBar : public sf::RectangleShape {
	private:
		int MAX_HITPOINTS;
		int hitpoints;
		sf::Vector2f position;
		float maxWidth;
		float maxHeight;

		void setColor();
	public:
		HealthBar(int hitpoints, int maxHitpoints, float x, float y, float width, float height = 5.0f);
		HealthBar(int hitpoints, int maxHitpoints, sf::Vector2f position, float width, float height = 5.0f);
		bool heal(int hitpoints);
		bool hit(int hitpoints);
		int getHitpoints();
};

#endif /* HEALTHBAR_HH_ */
