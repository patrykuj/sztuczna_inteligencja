#include "obstacle.hh"

#include <SFML/Graphics/Color.hpp>

Obstacle::Obstacle(float radius) :
		ObjectCircleShape(radius), texture(sf::Texture()) {
	texture.loadFromFile("img/obst.png");
}

Obstacle::Obstacle(sf::Vector2f position, float radius) :
		Obstacle(radius) {
	this->position.x = position.x;
	this->position.y = position.y;
	setPosition(position);
}

Obstacle::Obstacle(float x, float y, float radius) :
		Obstacle(radius) {
	this->position.x = x;
	this->position.y = y;
	setPosition(position);
}

void Obstacle::update() {
	setTexture(&texture);
}
