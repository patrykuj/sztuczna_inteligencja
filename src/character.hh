#ifndef CHARACTER_HH_
#define CHARACTER_HH_

#include <SFML/System/Vector2.hpp>

#include "healthbar.hh"
#include "object.hh"

const double DEGREES = 57.2957795f;

class Character : public Object {
	protected:
		sf::Vector2f velocity;
		float orientation;
		float angleularVelocity;
		float orientationAngle;
		HealthBar healthBar;
		virtual void updateHealthBar() = 0;
	public:
		Character(int hitpoints, int maxHitpoints, int x, int y, float width, float height = 5.0f);
		virtual ~Character();
		virtual void update() = 0;
		HealthBar getHealthBar();
		bool heal(int hitpoints);
		bool hit(int hitpoints);
		int getHitpoints();
};

#endif /* CHARACTER_HH_ */
