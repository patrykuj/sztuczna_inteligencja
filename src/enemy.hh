#ifndef ENEMY_HH_
#define ENEMY_HH_

#include <SFML/Graphics/Texture.hpp>
#include <SFML/System/Vector2.hpp>

#include "character.hh"
#include "objectcircleshape.hh"
#include "steeringBehaviour.hh"

class Enemy : public ObjectCircleShape, public Character {
	//friend class SteeringBehaviour;
	private:
		Enemy(float radius);
		SteeringBehaviour steeringBehaviour;
		sf::Texture texture;
	public:
		//sf::Vector2f wanderTarget;
		float wanderAngle;
		float dis;
		int botCounter;

		Enemy(sf::Vector2f position, float radius = 15.0f);
		Enemy(float x, float y, float radius = 15.0f);
		void update();
		void updateHealthBar();
		float GetMagnitude(sf::Vector2f vec);
		sf::Vector2f Normalize(sf::Vector2f vec);
		void attackCheck();
};

#endif /* ENEMY_HH_ */
