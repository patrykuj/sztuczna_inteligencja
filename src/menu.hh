#ifndef SRC_MENU_HH_
#define SRC_MENU_HH_

#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <string>

#include "menuenum.hh"
#include "state.hh"

const std::string NEW_GAME_TEXT = "New Game";
const std::string EXIT_TEXT = "Exit";
const sf::Color SELECTED(sf::Color::Red);
const sf::Color UNSELECTED(sf::Color::White);

class Menu : public State {
	private:
		sf::Text newGame;
		sf::Text exit;
		MenuEnum menuEnum;
		sf::Texture texture;
		sf::Sprite sprite;

		Menu(sf::RenderWindow *window = nullptr);

		void changeButton();
		void returnAction();
	public:
		static Menu& getInstance();
		~Menu();
		virtual void loop() override;
};

#endif /* SRC_MENU_HH_ */
