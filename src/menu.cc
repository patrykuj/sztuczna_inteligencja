#include "menu.hh"

#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Window/Keyboard.hpp>

#include "game.hh"
#include "globals.hh"
#include "status.hh"
#include "utils.hh"

Menu& Menu::getInstance() {
	static Menu instance;
	return instance;
}

Menu::Menu(sf::RenderWindow *window) :
		State(window), menuEnum(NEW_GAME) {
	newGame.setFont(font);
	newGame.setString(NEW_GAME_TEXT);
	newGame.setCharacterSize(FONT_SIZE);
	newGame.setColor(SELECTED);
	newGame.setPosition(20.0f, height - 120.0f - 20.0f);
	exit.setFont(font);
	exit.setString(EXIT_TEXT);
	exit.setCharacterSize(FONT_SIZE);
	exit.setColor(UNSELECTED);
	exit.setPosition(20.0f, height - 60.0f - 20.0f);
	texture.loadFromFile("img/b_menu.jpg", sf::IntRect(0, 0, width, height));
	texture.setSmooth(true);
	sprite.setTexture(texture);
}

Menu::~Menu() {

}

void Menu::loop() {
	sf::Event event;
	while(window->pollEvent(event)) {
		switch(event.type) {
			case sf::Event::Closed:
				window->close();
				break;
			case sf::Event::KeyReleased:
				switch(event.key.code) {
					case sf::Keyboard::Up:
					case sf::Keyboard::Down:
						changeButton();
						break;
					case sf::Keyboard::Return:
						returnAction();
						break;
					default:
						break;
				}
				break;
			default:
				break;
		}
	}
	window->draw(sprite);
	window->draw(newGame);
	window->draw(exit);
}

void Menu::changeButton() {
	switch(menuEnum) {
		case NEW_GAME:
			menuEnum = EXIT;
			newGame.setColor(UNSELECTED);
			exit.setColor(SELECTED);
			break;
		case EXIT:
			menuEnum = NEW_GAME;
			newGame.setColor(SELECTED);
			exit.setColor(UNSELECTED);
			break;
	}
}

void Menu::returnAction() {
	switch(menuEnum) {
		case NEW_GAME:
			prepareGame();
			gameStatus = GAME;
			break;
		case EXIT:
			window->close();
			break;
	}
}
