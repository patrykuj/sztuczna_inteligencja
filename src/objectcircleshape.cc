#include "objectcircleshape.hh"

#include <SFML/Graphics/Transform.hpp>

ObjectCircleShape::ObjectCircleShape(float radius, unsigned int pointCount) :
		sf::CircleShape(radius, pointCount), radius(radius) {
	setOrigin(radius, radius);
}

sf::Vector2f ObjectCircleShape::getVertexPoint(unsigned int index) {
	return getTransform().transformPoint(getPoint(index));
}

std::vector<sf::Vector2f> ObjectCircleShape::getVertex() {
	int pointCount = getPointCount();
	std::vector<sf::Vector2f> vertex;
	for(int i = 0; i < pointCount; ++i) {
		vertex.push_back(getVertexPoint(i));
	}
	return vertex;
}
