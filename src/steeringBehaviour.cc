#include "steeringBehaviour.hh"

#include <cmath>
#include <cstdlib>
#include <iterator>
#include <limits>
#include <vector>

#include "enemy.hh"
#include "globals.hh"
#include "obstacle.hh"
#include "player.hh"
#include "utils.hh"
#include "gameexception.hh"

SteeringBehaviour::SteeringBehaviour() {
	b = WANDER;
	destinationReach = true;
}

void SteeringBehaviour::wander(sf::Vector2f &velocity, float &wanderAngle, sf::Vector2f &position, sf::Vector2f bounce) {
	sf::Vector2f circleCenter = normalize(velocity);
	circleCenter = sf::Vector2f(circleCenter.x * 4, circleCenter.y * 4);

	sf::Vector2f displacement = sf::Vector2f(0, -2); //*3

	float dl = getMagnitude(displacement);
	displacement.x = sin(wanderAngle) * dl;
	displacement.y = -cos(wanderAngle) * dl;

	wanderAngle += random() * 0.01f - 0.01f * 0.02f;

	sf::Vector2f final = circleCenter + displacement;

	final = normalize(final);
	final = sf::Vector2f(final.x * 0.2f, final.y * 0.2f);

	velocity = velocity + final;
	endOfScreen(position, velocity, bounce);
//		velocity = normalize(velocity);
//		velocity = sf::Vector2f(velocity.x * ENEMY_SPEED, velocity.y * ENEMY_SPEED);
//
//		velocity = normalize(velocity + bounce);
	//}while( collisionTest(position + velocity) );

//	position = position + velocity;
}

void SteeringBehaviour::getHidingPosition(sf::Vector2f &position, sf::Vector2f &velocity, sf::Vector2f bounce) {	//131 PDF
	//find obstacle
	if(destinationReach) {
		float currentClosesObstacle = std::numeric_limits<float>::max();
		sf::Vector2f hideTarget;
		float targetRadius = 0.f;
		float distanceToNextObstacle;
		for(std::vector<Obstacle>::iterator it = obstacles.begin(); it != obstacles.end(); ++it) {
			distanceToNextObstacle = distance(position, it->getPosition());
			if(distanceToNextObstacle < currentClosesObstacle) {
				currentClosesObstacle = distanceToNextObstacle;
				hideTarget = it->getPosition();
				targetRadius = it->getRadius();
			}
		}
		float distAway = targetRadius + DISTANCE_FROM_BOUNDRY_HIDE;
		sf::Vector2f toObstacle = normalize(hideTarget - player.getPosition());

		destination = (toObstacle * distAway) + hideTarget;
		destinationReach = false;
	}
	//destination = sf::Vector2f(destination.x *)
	//velocity = normalize(position - destination) - velocity;
	velocity = normalize(destination - position);
//	velocity = normalize(velocity + bounce);
//	//position = position + velocity;
//	//position = (ToObstacle * distAway) + hideTarget;
//	position = position + velocity;
	endOfScreen(position, velocity, bounce);
	//position = destination;
}

void SteeringBehaviour::attack(sf::Vector2f &position, sf::Vector2f &velocity, sf::Vector2f bounce, sf::Vector2f playerPos, sf::Vector2f playerVelocity){
	sf::Vector2f toEvader = playerPos - position;
//
//	//double RelativeHeading =
	float lookAheadTime = getMagnitude(toEvader)/ 2.0f;//playerspeed + enemy speed

	sf::Vector2f final = sf::Vector2f(playerVelocity.x * lookAheadTime, playerVelocity.y * lookAheadTime);
	final = sf::Vector2f (playerPos + final);// * lookAheadTime;
//
	//velocity = normalize(position - final);
//
//	endOfScreen(position, velocity, bounce);
	velocity = normalize( final - position );
	endOfScreen(position, velocity, bounce);
}

void SteeringBehaviour::endOfScreen(sf::Vector2f &position, sf::Vector2f &velocity, sf::Vector2f bounce) {
	velocity = normalize(velocity);
	velocity = sf::Vector2f(velocity.x * ENEMY_SPEED, velocity.y * ENEMY_SPEED);

	velocity = normalize(velocity + bounce);
	position = position + velocity;

	if(position.x < ENEMY_SIZE || position.x >= width - ENEMY_SIZE) {
		velocity.x *= -1.05f;
		position.x += velocity.x;
	}
	if(position.y < ENEMY_SIZE || position.y >= height - ENEMY_SIZE) {
		velocity.y *= -1.05f;
		position.y += velocity.y;
	}

}

bool SteeringBehaviour::isDestinationReach(sf::Vector2f position, sf::Vector2f destination) {
	if(position.x + DESTINATION_REACH > destination.x && position.x - DESTINATION_REACH < destination.x)
		if(position.y + DESTINATION_REACH > destination.y && position.y - DESTINATION_REACH < destination.y)
			return true;
	return false;
}

bool SteeringBehaviour::collisionTest(sf::Vector2f position) {
	if(position.x - ENEMY_SIZE < 0.0f || position.x + ENEMY_SIZE > width || position.y - ENEMY_SIZE < 0.0f || position.y + ENEMY_SIZE > height)
		return true;

	for(std::vector<Obstacle>::iterator it = obstacles.begin(); it != obstacles.end(); ++it) {
		if(circleCollision(position, ENEMY_SIZE, it->getPosition(), it->getRadius(), 1.1f)) {
			return true;
		}
	}

	for(int i = 0; i < ENEMY_COUNT; ++i) {
		if(EnemyCollision(position, ENEMY_SIZE, enemies[i].getPosition(), ENEMY_SIZE, 1.1f) && position.x != enemies[i].getPosition().x && position.y != enemies[i].getPosition().y)
			return true;
	}
	return false;
}

bool SteeringBehaviour::EnemyCollision(sf::Vector2f positionA, float radiusA, sf::Vector2f positionB, float radiusB, float rad) {
	float radius = radiusA + radiusB;
	if(distance(positionA, positionB) < 3)
		return false;
	if(distance(positionA, positionB) > radius * rad)
		return false;
	return true;
}

sf::Vector2f SteeringBehaviour::getVectorFieldValue(sf::Vector2f &position) {
	sf::Vector2f vectorFieldValue(0.0f, 0.0f);
	float radiusEnemy = ENEMY_SIZE * ENEMY_SIZE;

	for(std::vector<Obstacle>::iterator it = obstacles.begin(); it != obstacles.end(); ++it) {
		float radiusObstacle = it->getRadius();
		sf::Vector2f obstaclePosition = it->getPosition();
		if(circleCollision(position, ENEMY_SIZE, obstaclePosition, radiusObstacle)) {
			radiusObstacle *= radiusObstacle;
			float radiusSum = radiusObstacle + radiusEnemy;
			float dirX = position.x - obstaclePosition.x;
			float dirY = position.y - obstaclePosition.y;
			float dis = dirX * dirX + dirY * dirY - radiusSum;
			dis = sqrtf(dis);
			float magnitude = radiusObstacle / dis;
			vectorFieldValue.x += (dirX / dis) * magnitude;
			vectorFieldValue.y += (dirY / dis) * magnitude;
		}
	}

	for(std::vector<Enemy>::iterator it = enemies.begin(); it != enemies.end(); ++it) {
		sf::Vector2f enemyPosition = it->getPosition();
		if(position != enemyPosition && circleCollision(position, ENEMY_SIZE, enemyPosition, ENEMY_SIZE)) {
			float radiusSum = radiusEnemy + radiusEnemy;
			float dirX = position.x - enemyPosition.x;
			float dirY = position.y - enemyPosition.y;
			float dis = dirX * dirX + dirY * dirY - radiusSum;
			dis = sqrtf(dis);
			float magnitude = radiusEnemy / dis;
			vectorFieldValue.x += (dirX / dis) * magnitude;
			vectorFieldValue.y += (dirY / dis) * magnitude;
		}
	}

//	std::vector<sf::Vector2f> vertex = player.getVertex();
//	if(triangleCircleCollision(vertex, position, ENEMY_SIZE)) {
//		player.hit(rand() % (MAX_ENEMY_HITPOINT - MIN_ENEMY_HITPOINT) + MIN_ENEMY_HITPOINT);
//					if(player.getHitpoints() <= 0)
//						throw GameException("Koniec gry");
//		float radiusSum = PLAYER_SIZE * PLAYER_SIZE + radiusEnemy;
//		float dirX = player.getPosition().x - position.x;
//		float dirY = player.getPosition().y - position.y;
//		float dis = dirX * dirX + dirY * dirY - radiusSum;
//		dis = sqrtf(dis);
//		float magnitude = radiusEnemy / dis;
//		vectorFieldValue.x += (dirX / dis) * magnitude;
//		vectorFieldValue.y += (dirY / dis) * magnitude;
//	}

	float dis = sqrtf(vectorFieldValue.x * vectorFieldValue.x + vectorFieldValue.y * vectorFieldValue.y);

	if(dis > 2.0f) {
		vectorFieldValue.x = (vectorFieldValue.x / dis) * SQRT2;
		vectorFieldValue.y = (vectorFieldValue.y / dis) * SQRT2;
	}

	return vectorFieldValue;
}
