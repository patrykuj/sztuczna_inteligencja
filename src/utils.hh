#ifndef UTILS_HH_
#define UTILS_HH_

#include <SFML/System/Vector2.hpp>
#include <vector>

#include "bullet.hh"
#include "obstacle.hh"

sf::Vector2f getRandomPosition(int width, int height);
float distance(sf::Vector2f a, sf::Vector2f b);
bool circleCollision(sf::Vector2f positionA, float radiusA, sf::Vector2f positionB, float radiusB, float scale = 1.0f);
bool checkObstacle(std::vector<Obstacle> obstacle, sf::Vector2f position, float radius);
bool triangleCircleCollision(std::vector<sf::Vector2f> &triangle, sf::Vector2f circleCenter, float radius);
bool rectangleCircleCollision(std::vector<sf::Vector2f> &rectangle, sf::Vector2f circleCenter, float radius);
float fRand(float fMin, float fMax);
void prepareObstacle();
void preparePlayer();
void prepareEnemy();
void prepareGame();
void updateEnemy();
void updateBullet();
void updateObstacle();
void updateGame();
float getMagnitude(sf::Vector2f velocity);
sf::Vector2f normalize(sf::Vector2f e);

#endif /* UTILS_HH_ */
