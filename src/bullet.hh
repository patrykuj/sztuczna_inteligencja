#ifndef SRC_BULLET_HH_
#define SRC_BULLET_HH_

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/System/Vector2.hpp>

class Bullet : public sf::RectangleShape {
	private:
		sf::Vector2f position;
		sf::Vector2f velocity;
		float rotation;
	public:
		Bullet();
		Bullet(sf::Vector2f position, float rotation);
		Bullet(float positionX, float positionY, float rotation);
		bool update();
};

#endif /* SRC_BULLET_HH_ */
