#include "healthbar.hh"

#include <SFML/Graphics/Color.hpp>
#include <iostream>

HealthBar::HealthBar(int hitpoints, int maxHitpoints, sf::Vector2f position, float width, float height) :
		sf::RectangleShape(sf::Vector2f(width, height)), MAX_HITPOINTS(maxHitpoints + 1), position(position), maxWidth(width), maxHeight(height) {
	this->hitpoints = hitpoints % MAX_HITPOINTS;
	setOrigin(width / 2, height / 2);
	setFillColor(sf::Color::Green);
}

HealthBar::HealthBar(int hitpoints, int maxHitpoints, float x, float y, float width, float height) :
		sf::RectangleShape(sf::Vector2f(width, height)), MAX_HITPOINTS(maxHitpoints + 1), maxWidth(width), maxHeight(height) {
	this->position = sf::Vector2f(x, y);
	this->hitpoints = hitpoints % MAX_HITPOINTS;
	setOrigin(width / 2, height / 2);
	setFillColor(sf::Color::Green);
}

bool HealthBar::heal(int hitpoints) {
	if(hitpoints < 0)
		hitpoints = 0;
	this->hitpoints = this->hitpoints + hitpoints % MAX_HITPOINTS;
	setSize(sf::Vector2f(maxWidth * this->hitpoints / MAX_HITPOINTS, maxHeight));
	setColor();
	if(this->hitpoints == MAX_HITPOINTS)
		return true;
	else
		return false;
}

bool HealthBar::hit(int hitpoints) {
	if(hitpoints < 0)
		hitpoints = 0;
	this->hitpoints -= hitpoints;
	bool isDead = false;
	if(this->hitpoints <= 0) {
		this->hitpoints = 0;
		isDead = true;
	}
	setSize(sf::Vector2f(maxWidth * this->hitpoints / MAX_HITPOINTS, maxHeight));
	setColor();
	return isDead;
}

int HealthBar::getHitpoints() {
	return hitpoints;
}

void HealthBar::setColor() {
	float hitpointsRatio = hitpoints / static_cast<float>(MAX_HITPOINTS);
	if(hitpointsRatio > 0.6)
		setFillColor(sf::Color::Green);
	else if(hitpointsRatio > 0.2)
		setFillColor(sf::Color::Yellow);
	else {
		setFillColor(sf::Color::Red);
	}
}
