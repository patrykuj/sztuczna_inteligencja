#ifndef SRC_STATE_HH_
#define SRC_STATE_HH_

#include <SFML/Graphics/RenderWindow.hpp>

class State {
	protected:
		sf::RenderWindow *window;

		State(sf::RenderWindow *window = nullptr);
	public:
		virtual ~State();
		virtual void loop() = 0;
		void setWindows(sf::RenderWindow *window);
};

#endif /* SRC_STATE_HH_ */
