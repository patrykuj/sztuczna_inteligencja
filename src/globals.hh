#ifndef GLOBALS_HH_
#define GLOBALS_HH_

#include <SFML/Graphics/Font.hpp>
#include <SFML/System/Vector2.hpp>
#include <vector>

#include "bullet.hh"
#include "enemy.hh"
#include "obstacle.hh"
#include "player.hh"
#include "status.hh"

extern float width;
extern float height;
extern std::vector<Obstacle> obstacles;
extern std::vector<Enemy> enemies;
extern std::vector<Bullet> bullets;
extern Player player;
extern sf::Font font;

const float PLAYER_SIZE = 15.0f;
const float ENEMY_SIZE = 15.0f;
const float HEALTH_BAR_OFFSET = 5.0f;
const float BULLET_THICKNESS = 2.0f;
const float BULLET_LENGTH = 10.0f;
const float BULLET_SPEED = 4.0f;
const sf::Vector2f BULLET_SIZE(BULLET_THICKNESS, BULLET_LENGTH);

const int ENEMY_SAFE_ZONE = 200;
const int SIZE_OF_PLAYER_SAFE_RESAWN_ZONE = 350;
const float DISTANCE_FROM_BOUNDRY_HIDE = ENEMY_SIZE + 15.0f;
const int ENEMY_COUNT = 10;
const int WANDERCIRCERADIUS = 20;
const float ENEMY_SPEED = 1.0f;
const int MAX_BULLET_HITPOINT = 40;
const int MIN_BULLET_HITPOINT = 10;
const int MAX_ENEMY_HITPOINT = 10;
const int MIN_ENEMY_HITPOINT = 5;
const float SQRT2 = 1.414f;
const float DESTINATION_REACH = 10.0f;
const int ATTACK_CIRCLE = 50;
const float PLAYER_MAX_SPEED = 1.225f;

const unsigned int FONT_SIZE = 60;

extern Status gameStatus;

#endif /* GLOBALS_HH_ */
