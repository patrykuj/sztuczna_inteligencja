#include "gameexception.hh"

GameException::GameException(const std::string& what_arg) :
		std::runtime_error(what_arg) {
}

GameException::GameException(const char* what_arg) :
		std::runtime_error(what_arg) {
}
