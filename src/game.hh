#ifndef GAME_HH_
#define GAME_HH_

#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <string>
#include <vector>

#include "state.hh"

const std::string TEXT = "Game Over";
const sf::Color TEXT_COLOR(sf::Color::Red);

class Game : public State {
	private:
		Game(sf::RenderWindow *window = nullptr);
		std::vector<sf::Texture> textures;
		sf::Sprite sprite;
		sf::Text gameOver;
		bool end;
	public:
		static Game& getInstance();
		~Game();
		virtual void loop() override;
		void setEnd(bool end);
		void randomTexture();
};

#endif /* GAME_HH_ */
