#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/ContextSettings.hpp>
#include <SFML/Window/VideoMode.hpp>
#include <SFML/Window/WindowStyle.hpp>
#include <cstdlib>
#include <ctime>

#include "game.hh"
#include "globals.hh"
#include "menu.hh"
#include "status.hh"
#include "utils.hh"

float width;
float height;
sf::Font font;
Status gameStatus;

int main() {
	font.loadFromFile("font/STONB.TTF");
	srand(time(nullptr));
	sf::VideoMode videoMode = sf::VideoMode::getDesktopMode();
	width = videoMode.width;
	height = videoMode.height;
	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;
	sf::RenderWindow window(sf::VideoMode(width, height, 32), "Gra", sf::Style::Fullscreen, settings);
	window.setFramerateLimit(60);
	Menu menu = Menu::getInstance();
	Game game = Game::getInstance();
	menu.setWindows(&window);
	game.setWindows(&window);
	gameStatus = MENU;

	while(window.isOpen()) {
		window.clear(sf::Color::Black);

		switch(gameStatus) {
			case MENU:
				menu.loop();
				game.setEnd(false);
				game.randomTexture();
				break;
			case GAME:
				game.loop();
				break;
		}

		window.display();
	}

	return 0;
}
