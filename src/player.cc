#include "player.hh"

#include <SFML/Graphics/Color.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <cmath>
#include <cstdlib>
#include <iterator>
#include <vector>

#include "bullet.hh"
#include "enemy.hh"
#include "gameexception.hh"
#include "globals.hh"
#include "healthbar.hh"
#include "obstacle.hh"
#include "utils.hh"

Player::Player(float radius) :
		ObjectCircleShape(radius, 3), Character(100, 100, 0, 0, 2 * radius), texture(sf::Texture()) {
	texture.loadFromFile("img/player.png");
}

Player::Player(sf::Vector2f position, float radius) :
		Player(radius) {
	this->position = position;
	this->healthBar.setPosition(position.x, position.y - radius - HEALTH_BAR_OFFSET);
	setPosition(this->position);
}

Player::Player(float x, float y, float radius) :
		Player(radius) {
	this->position = sf::Vector2f(x, y);
	this->healthBar.setPosition(x, y - radius - HEALTH_BAR_OFFSET);
	setPosition(this->position);
}

Player::Player() :
		Player(0, 0) {
}

void Player::update() {
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
		velocity.x += sin(orientation) * 0.1f;
		velocity.y -= cos(orientation) * 0.1f;
	}

	if(sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
		velocity.x -= sin(orientation) * 0.1f;
		velocity.y += cos(orientation) * 0.1f;
	}

	if(sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
		angleularVelocity -= 0.003f;
	}

	if(sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
		angleularVelocity += 0.003f;
	}

	orientation += angleularVelocity;
	orientationAngle = orientation * DEGREES;

	angleularVelocity *= 0.95f;
	velocity.y *= 0.95f;
	velocity.x *= 0.95f;

	float lenVelocity = sqrtf(velocity.x * velocity.x + velocity.y * velocity.y);

	if(lenVelocity > 1.5f) {
		velocity.x /= lenVelocity;
		velocity.x *= PLAYER_MAX_SPEED;
		velocity.y /= lenVelocity;
		velocity.y *= PLAYER_MAX_SPEED;
	}

	sf::Vector2f vectorFieldValue = getVectorFieldValue();

	if(vectorFieldValue.x != 0 || vectorFieldValue.y != 0) {
		velocity.x = vectorFieldValue.x;
		velocity.y = vectorFieldValue.y;
	}

	position.x += velocity.x;
	position.y += velocity.y;

	if(position.x < radius || position.x >= width - radius) {
		velocity.x *= -1.05f;
		position.x += velocity.x;
	}
	if(position.y < radius || position.y >= height - radius) {
		velocity.y *= -1.05f;
		position.y += velocity.y;
	}

//	position.y = ((int) (height * 1000 + position.y * 1000) % (int) (height * 1000)) / 1000.0f;
//	position.x = ((int) (width * 1000 + position.x * 1000) % (int) (width * 1000)) / 1000.0f;

	setRotation(orientationAngle);
	setPosition(position);
	updateHealthBar();
	setTexture(&texture);

	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
		if(timer.getElapsedTime() > frequency) {
			bullets.push_back(Bullet(getVertexPoint(0), orientationAngle));
			timer.restart();
		}
	}
}

sf::Vector2f Player::getVectorFieldValue() {
	sf::Vector2f position = getPosition();
	sf::Vector2f vectorFieldValue(0.0f, 0.0f);

	std::vector<sf::Vector2f> vertex = getVertex();

	for(std::vector<Obstacle>::iterator it = obstacles.begin(); it != obstacles.end(); ++it) {
		float radiusObstacle = it->getRadius();
		sf::Vector2f obstaclePosition = it->getPosition();
		if(triangleCircleCollision(vertex, obstaclePosition, radiusObstacle)) {
			radiusObstacle *= radiusObstacle;
			float radiusSum = radiusObstacle + radius * radius;
			float dirX = position.x - obstaclePosition.x;
			float dirY = position.y - obstaclePosition.y;
			float dis = dirX * dirX + dirY * dirY - radiusSum;
			dis = sqrtf(dis);
			float magnitude = radiusObstacle / dis;
			vectorFieldValue.x += (dirX / dis) * magnitude;
			vectorFieldValue.y += (dirY / dis) * magnitude;
		}
	}

	for(std::vector<Enemy>::iterator it = enemies.begin(); it != enemies.end(); ++it) {
		float radiusEnemy = it->getRadius();
		sf::Vector2f enemyPosition = it->getPosition();
		if(triangleCircleCollision(vertex, enemyPosition, radiusEnemy)) {
			hit(rand() % (MAX_ENEMY_HITPOINT - MIN_ENEMY_HITPOINT) + MIN_ENEMY_HITPOINT);
			if(getHitpoints() <= 0)
				throw GameException("Koniec gry");
			radiusEnemy *= radiusEnemy;
			float radiusSum = radiusEnemy + radius * radius;
			float dirX = position.x - enemyPosition.x;
			float dirY = position.y - enemyPosition.y;
			float dis = dirX * dirX + dirY * dirY - radiusSum;
			dis = sqrtf(dis);
			float magnitude = radiusEnemy / dis;
			vectorFieldValue.x += (dirX / dis) * magnitude;
			vectorFieldValue.y += (dirY / dis) * magnitude;
		}
	}

	float dis = sqrtf(vectorFieldValue.x * vectorFieldValue.x + vectorFieldValue.y * vectorFieldValue.y);

	if(dis > 2.0f) {
		vectorFieldValue.x = (vectorFieldValue.x / dis) * SQRT2;
		vectorFieldValue.y = (vectorFieldValue.y / dis) * SQRT2;
	}

	return vectorFieldValue;
}

sf::Vector2f Player::getVelocity() {
	return velocity;
}

void Player::updateHealthBar() {
	float barY = position.y - radius - HEALTH_BAR_OFFSET;
	healthBar.setPosition(position.x, barY > 0 ? barY : position.y + radius + HEALTH_BAR_OFFSET);
}
