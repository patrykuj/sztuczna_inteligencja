#include "game.hh"

#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <iterator>
#include <vector>

#include "bullet.hh"
#include "enemy.hh"
#include "gameexception.hh"
#include "globals.hh"
#include "healthbar.hh"
#include "obstacle.hh"
#include "player.hh"
#include "status.hh"
#include "utils.hh"

Game::Game(sf::RenderWindow *window) :
		State(window), end(false) {
	textures.push_back(sf::Texture());
	textures[0].loadFromFile("img/b1.jpg", sf::IntRect(0, 0, width, height));
	textures[0].setSmooth(true);
	textures.push_back(sf::Texture());
	textures[1].loadFromFile("img/b2.jpg", sf::IntRect(0, 0, width, height));
	textures[1].setSmooth(true);
	textures.push_back(sf::Texture());
	textures[2].loadFromFile("img/b3.jpg", sf::IntRect(0, 0, width, height));
	textures[2].setSmooth(true);
	textures.push_back(sf::Texture());
	textures[3].loadFromFile("img/b4.jpg", sf::IntRect(0, 0, width, height));
	textures[3].setSmooth(true);
	gameOver.setColor(TEXT_COLOR);
	gameOver.setString(TEXT);
	gameOver.setFont(font);
	gameOver.setCharacterSize(FONT_SIZE);
	sf::FloatRect rect = gameOver.getLocalBounds();
	gameOver.setOrigin(rect.width / 2.0f, rect.height / 2.0f);
	gameOver.setPosition(width / 2.0f, height / 2.0f);
}

Game& Game::getInstance() {
	static Game instance;
	return instance;
}

Game::~Game() {
}

void Game::loop() {
	sf::Event event;
	while(window->pollEvent(event)) {
		switch(event.type) {
			case sf::Event::Closed:
				window->close();
				break;
			case sf::Event::KeyPressed:
				switch(event.key.code) {
					case sf::Keyboard::Escape:
						gameStatus = MENU;
						break;
					default:
						break;
				}
				break;
			default:
				break;
		}
	}
	if(!end) {
		try {
			updateGame();
		} catch(GameException& gameException) {
			end = true;
		}
	}
	window->draw(sprite);

	for(std::vector<Obstacle>::iterator it = obstacles.begin(); it != obstacles.end(); ++it) {
		window->draw(*it);
	}

	for(std::vector<Enemy>::iterator it = enemies.begin(); it != enemies.end(); ++it) {
		window->draw(*it);
		window->draw(it->getHealthBar());
	}

	window->draw(player);
	window->draw(player.getHealthBar());

	for(std::vector<Bullet>::iterator it = bullets.begin(); it != bullets.end(); ++it) {
		window->draw(*it);
	}

	if(end)
		window->draw(gameOver);
}

void Game::setEnd(bool end) {
	this->end = end;
}

void Game::randomTexture() {
	sprite.setTexture(textures[rand() % textures.size()]);
}
