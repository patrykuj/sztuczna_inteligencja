################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/enemy.cc \
../src/game.cc \
../src/objectcircleshape.cc \
../src/obstacle.cc \
../src/player.cc \
../src/utils.cc 

CC_DEPS += \
./src/enemy.d \
./src/game.d \
./src/objectcircleshape.d \
./src/obstacle.d \
./src/player.d \
./src/utils.d 

OBJS += \
./src/enemy.o \
./src/game.o \
./src/objectcircleshape.o \
./src/obstacle.o \
./src/player.o \
./src/utils.o 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


